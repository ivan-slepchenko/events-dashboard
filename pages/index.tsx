import type {NextPage} from 'next'
import {useEvents, getServerSideProps as _getServerSideProps, EventStatusEnum} from "../stores/events.store";
import {EventCardComponent} from "../components/event-card/event-card.component";
import {useEffect, useRef, useState} from "react";
import {auditTime, combineLatest, filter, Subject} from "rxjs";
import {EventData} from "../stores/types";

export const getServerSideProps = _getServerSideProps;
export const onCardGotIntoVisibleArea$$ = new Subject<string>();

const Home: NextPage = () => {
	const {
		eventsData$,
		total$,
		perPage$,
		page$,
		eventStatus$,
		setPageToNext,
		setEventStatus,
	} = useEvents();
	const eventsContainer = useRef<HTMLDivElement>(null)
	const isError = false;
	const isLoading = false;

	const [eventsData, setEventsData] = useState<{eventStatus: string, events: EventData[]} | null>(null);
	const [localEventStatus, setLocalEventStatus] = useState<EventStatusEnum>(EventStatusEnum.Upcoming);

	useEffect(() => {
		const subscriptions=[
			eventStatus$.subscribe(setLocalEventStatus),
			eventsData$.subscribe((eventsData) => {
				setEventsData(eventsData);
			}),
			combineLatest([eventsData$, page$, onCardGotIntoVisibleArea$$, perPage$, total$, eventStatus$]).pipe(
				auditTime(100),
				filter(([eventsData,,,,,eventStatus]) => eventsData.eventStatus === eventStatus),
				filter(([eventsData, page, newCardIdInVisibleArea, perPage, total, eventStatus]) => {
					const index = eventsData.events!.indexOf(eventsData.events!.find(event => event.id === newCardIdInVisibleArea)!);

					return (eventsContainer.current != null &&
						page != null &&
						perPage != null &&
						total != null &&
						(index >= page * perPage - 5) &&
						(page * perPage < total));
				})
			).subscribe(([,page,,,,]) => {
				setPageToNext();
			})
		]
		return () => {
			subscriptions.forEach(subscription => subscription.unsubscribe());
		}
	}, [])

	const tabButton = (status: EventStatusEnum) => {
		return (
			<button
				className={`text-gray-600 py-4 px-6 block hover:text-blue-500 focus:outline-none ${localEventStatus === status ? 'text-blue-500 border-b-2 font-medium border-blue-500' : ''}`}
				onClick={() => {
					eventsContainer.current!.scroll({
						top: 0
					});
					setEventsData(null);
					setEventStatus(status)
				}}
			>
				{status.toUpperCase()}
			</button>
		);
	};

	return (
		<div className="w-full h-full absolute flex flex-col">
			<div className="bg-white">
				<nav className="flex justify-center flex-row">
					{tabButton(EventStatusEnum.All)}
					{tabButton(EventStatusEnum.Upcoming)}
					{tabButton(EventStatusEnum.Live)}
					{tabButton(EventStatusEnum.Ended)}
				</nav>
			</div>
			{isError && <div>Something went wrong ...</div>}
			<div ref={eventsContainer} className="jq-events-container w-full h-full overflow-auto">
				{isLoading ? (
					<div>Loading ...</div>
				) : (
					<div className="mx-auto flex flex-col items-center justify-center px-8">
						{eventsData && eventsData.events.map((event) => (
							<EventCardComponent eventData={event} onCardGotIntoVisibleArea$$={onCardGotIntoVisibleArea$$} key={event.id}/>
						))}
					</div>
				)}
			</div>
		</div>
	);
}

export default Home
