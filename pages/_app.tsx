import '../styles/globals.css'
import type { AppProps } from 'next/app'
import {EventsProvider} from "../stores/events.store";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <EventsProvider firstPageEventsData={pageProps.firstPageEventsData} total={pageProps.total}>
      <Component {...pageProps} />
    </EventsProvider>
  )
}

export default MyApp
