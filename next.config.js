/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['stonks-app.s3.amazonaws.com'],
  },
}

module.exports = nextConfig
