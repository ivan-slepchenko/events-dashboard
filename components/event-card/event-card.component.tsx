import {DetailedHTMLProps, FC, HTMLAttributes, useEffect} from "react";
import {EventData} from "../../stores/types";
import {distinct, filter, interval, map, Subject} from "rxjs";
import $ from "jquery";
import moment from "moment";

export interface EventCardProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	eventData: EventData;
	onCardGotIntoVisibleArea$$: Subject<string>;
}

const isScrolledIntoView = (elementSelector: string): boolean => {
	const container = $(".jq-events-container");
	const element = $(elementSelector);
	const containerTop = container.offset()!.top;
	const containerBottom = containerTop + container.height()!;
	const elemTop = element.offset()!.top;
	const elemBottom = elemTop + element.height()!;

	return (
		(elemTop <= containerTop && elemBottom >= containerTop) ||
		(elemTop <= containerBottom && elemBottom >= containerBottom) ||
		(elemTop >= containerTop && elemBottom <= containerBottom)
	);
}

export const EventCardComponent: FC<EventCardProps> = ({eventData, onCardGotIntoVisibleArea$$}) => {
	const eventClass = 'event-card-' + eventData.id;

	useEffect(() => {
		const scroll$ = interval(300).pipe(
			map(() => isScrolledIntoView('.' + eventClass)),
			filter(isScrolledIntoView => isScrolledIntoView),
			distinct(),
		).subscribe(() => {
			scroll$.unsubscribe();
			onCardGotIntoVisibleArea$$.next(eventData.id);
		});
		return () => scroll$.unsubscribe();
	}, []);

	return (
		<div className={"flex flex-col w-full bg-white rounded-lg mt-4 shadow-lg sm:w-3/4 md:w-1/2 lg:w-3/5 " + eventClass} key={eventData.id}>
			<img
				className="w-full rounded-t-lg hidden"
				src={eventData.hero_url}
				alt={eventData.name}
				onLoad={(e) => {
					(e.target as HTMLImageElement).classList.remove('hidden');
				}}
			/>
			<div className="flex flex-col w-full md:flex-row">
				<div
					className="flex flex-row justify-around p-4 font-bold leading-none text-gray-800 uppercase bg-blue-400 md:flex-col md:items-center md:justify-center md:w-1/4">
					<div className="md:text-3xl">{moment(eventData.starts_at).format("MMM")}</div>
					<div className="md:text-6xl">{moment(eventData.starts_at).format("Do")}</div>
					<div className="md:text-xl">{moment(eventData.starts_at).format("h a")}</div>
				</div>
				<div className="p-4 font-normal text-gray-800 md:w-3/4">
					<h1 className="mb-4 text-4xl font-bold leading-none tracking-tight text-gray-800">{eventData.name}</h1>
					<p className="leading-normal">{eventData.description}</p>
					<div className="flex flex-row items-center mt-4 text-gray-700">
						<div className="w-1/2">
							{eventData.partner.name}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
