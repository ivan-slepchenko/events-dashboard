import {createContext, useContext, FC, ReactElement} from "react";
import {EventData, EventsPageData} from "./types";
import {
	auditTime,
	BehaviorSubject,
	combineLatest,
	concatAll, filter,
	firstValueFrom,
	from,
	map,
	scan,
	share,
	tap
} from "rxjs";
import {fromFetch} from "rxjs/fetch";
import {GetServerSideProps} from "next";

export enum EventStatusEnum {
  	All= "all",
	Upcoming = "upcoming",
	Live = "live",
	Ended = "ended",
}
enum OrderByEnum {
  StartsAt_ASC = "starts_at,ASC",
}
const getURL = (eventStatus: EventStatusEnum, perPage: number, orderBy:string, page: number):string => {
	return eventStatus === EventStatusEnum.All
		? `https://api.stonks.com/principal/event?perPage=${perPage}&orderBy=${orderBy}&page=${page}`
		: `https://api.stonks.com/principal/event?perPage=${perPage}&orderBy=${orderBy}&page=${page}&status=${eventStatus}`
}
const initialEventStatus = EventStatusEnum.Upcoming;
const orderBy$$ = new BehaviorSubject<string>(OrderByEnum.StartsAt_ASC);
const perPage$$ = new BehaviorSubject<number>(10);
const page$$ = new BehaviorSubject<number>(1);
const total$$ = new BehaviorSubject<number>(0);
const eventStatus$$ = new BehaviorSubject<EventStatusEnum>(initialEventStatus);

const eventsData$ = combineLatest([eventStatus$$, orderBy$$, perPage$$, page$$]).pipe(
	auditTime(0),
	map(([eventStatus, orderBy,perPage,page]) => combineLatest([from([eventStatus]), fromFetch(getURL(eventStatus, perPage, orderBy, page))])),
	concatAll(),
	map(([eventStatus, res]) => combineLatest([from([eventStatus]), from(res.json() as Promise<EventsPageData>)])),
	concatAll(),
	share(),
	map(([eventStatus, eventsPageData]) => {
		eventsPageData.eventStatus = eventStatus;
		return eventsPageData;
	}),
	tap((eventsPageData) => {
		total$$.next(eventsPageData.meta.total);
	}),
)

export const getServerSideProps:GetServerSideProps  = async () => {
	const firstPageEventsData = await firstValueFrom(eventsData$);
	const total = await firstValueFrom(total$$);
	return {
		props: {
			firstPageEventsData: firstPageEventsData,
			total,
		},
	};
}

const useEventsController = (firstPageEventsData: EventsPageData, total: number) => {
	total$$.next(total);
	return {
		setOrderBy: (orderBy: string) => {
			orderBy$$.next(orderBy);
		},
		setPageToNext: () => {
			page$$.next(page$$.value + 1);
		},
		setEventStatus: (eventStatus: EventStatusEnum) => {
			page$$.next(1);
			eventStatus$$.next(eventStatus);
		},
		eventsData$: page$$.pipe(
			auditTime(10),
			map(page => (page === 1 && eventStatus$$.value === initialEventStatus) ?
				from([firstPageEventsData]) :
				eventsData$.pipe(filter(eventsPageData => {
					return eventsPageData.eventStatus === eventStatus$$.value
				}))
			),
			concatAll(),
			scan((acc:{eventStatus: string, events: EventData[]}, curr:EventsPageData) => {
				const page = curr.meta.current_page;
				const accumulatedEventStatus = acc.eventStatus;
				const accumulatedEvents = acc.events;
				if(accumulatedEventStatus === eventStatus$$.value){
					return {eventStatus: accumulatedEventStatus, events: [...(page === 1 ? [] : accumulatedEvents), ...curr.data]};
				} else {
					return {eventStatus: eventStatus$$.value, events: curr.data};
				}

			}, {eventStatus: initialEventStatus, events: []}),
		),
		total$: total$$.asObservable(),
		page$: page$$.asObservable().pipe(share()),
		perPage$: perPage$$.asObservable(),
		eventStatus$: eventStatus$$.asObservable(),
	};
};

const EventsContext = createContext<ReturnType<typeof useEventsController>>({
	setOrderBy: () => {},
	setPageToNext: () => {},
	setEventStatus: () => {},
	eventsData$: from([{eventStatus: initialEventStatus, events: []}]),
	total$: from([]),
	page$: from([]),
	perPage$: from([]),
	eventStatus$: from([]),
});

type EventsProviderProps = {
	firstPageEventsData: EventsPageData;
	total: number;
	children?:
	| ReactElement
	| ReactElement[];
};

export const EventsProvider:FC<EventsProviderProps> = ({ firstPageEventsData, total, children }) => (
	<EventsContext.Provider value={useEventsController(firstPageEventsData, total)}>
		{children}
	</EventsContext.Provider>
);

export const useEvents = () => useContext(EventsContext);
