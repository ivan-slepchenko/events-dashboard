export interface Panelist {
	id: string;
	name: string;
	company: string;
	title: string;
	email: string;
	avatar_url: string;
	twitter_url: string;
	linkedin_url: string;
}

export interface InvestmentInterest {
	id: number;
	text: string;
	icon: string;
}

export interface Location {
	id: number;
	name: string;
}

export interface Partner {
	id: number;
	name: string;
	slug: string;
	website: string;
	logo_url: string;
	description: string;
	hero_url: string;
	hero_color?: any;
	commit_check_sizes: number[];
	intro_check_sizes: number[];
	carry: number;
	early_bird: number;
	early_bird_allocation: number;
	is_trial: boolean;
	followers_count: number;
	is_following: boolean;
	total_interest: number;
	intros_count: number;
	commits_count: number;
	investment_interests: InvestmentInterest[];
	locations: Location[];
}


export interface EventData {
	id: string;
	slug: string;
	name: string;
	description: string;
	short_description: string;
	hero_url: string;
	stream_url: string;
	status: number;
	starts_at: Date;
	ends_at: Date;
	investments_end_at: Date;
	is_attending: boolean;
	rsvp_link?: any;
	has_custom_rsvp: boolean;
	unlisted: boolean;
	private: boolean;
	schedule?: any;
	google_studio_embed_url?: any;
	type: string;
	require_approval: boolean;
	guests_limit?: number;
	featured: boolean;
	commit_check_sizes: number[];
	intro_check_sizes: number[];
	is_draft: boolean;
	carry: number;
	early_bird: number;
	early_bird_allocation: number;
	is_test: boolean;
	rsvps_number: number;
	rsvp_threshold: number;
	has_rsvp_invitation: boolean;
	panelists: Panelist[];
	partner: Partner;
	investment_interests: InvestmentInterest2[];
}

export interface Links {
	first: string;
	last: string;
	prev?: any;
	next?: any;
}

export interface Link {
	url: string;
	label: string;
	active: boolean;
}

export interface Meta {
	current_page: number;
	from: number;
	last_page: number;
	links: Link[];
	path: string;
	per_page: string;
	to: number;
	total: number;
}

export interface EventsPageData {
    eventStatus?: string;
	data: EventData[];
	links: Links;
	meta: Meta;
}


